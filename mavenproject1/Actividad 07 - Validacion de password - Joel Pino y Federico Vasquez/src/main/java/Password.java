
public class Password {

    private static int max;
    private static int min;
    private static boolean digito;

    public static void validarPassword(String password) throws PasswordInvalidoException {
        min = 8;
        max = 20;
        if (password == null) {
            throw new IllegalArgumentException("La password ingresada es nula");
        }
        if (password.equals("")) {
            throw new IllegalArgumentException("La password ingresado es nulo o vacio");
        }
        for (int i = 0; i < password.length(); i++) {
            if (Character.isDigit(password.charAt(i))) {
                digito = true;
            }
        }
        if (password.length() < min) {
            if (digito == true) {/* si es menor y tiene digito*/
                throw new PasswordInvalidoException("La password es menor y tiene digito");
            } else /* si es menor y no tiene digito*/ {
                throw new PasswordInvalidoException("La password es menor y no tiene digito");
            }
        }
        if (password.length() > max) {/* si es mayor y tiene digito*/
            if (digito == true) {
                throw new PasswordInvalidoException("La password es mayor y tiene digito");
            } else /* si es mayor y no tiene digito*/ {
                throw new PasswordInvalidoException("La password es mayor y no tiene digito");
            }
        }
        if (password.length() > min && password.length() < max) {
            if (digito == true) {
                throw new PasswordInvalidoException("la password utilizada es valida");
            } else {
                throw new PasswordInvalidoException("la password utilizada no cointiene digitos");
            }
        }
    }

    public static void main(String[] args) {
        try {
            Password.validarPassword("asdasasdd");
            System.out.print("la password utilizada es valida");
        } catch (PasswordInvalidoException e) {
            System.out.print(e.getMessage());
        }
    }
}
