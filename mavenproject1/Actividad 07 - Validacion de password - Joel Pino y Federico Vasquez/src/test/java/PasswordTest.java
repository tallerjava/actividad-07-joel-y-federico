
import org.junit.Test;

public class PasswordTest {

    Password password = new Password();

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordVacio_IllegalArgumentException() throws PasswordInvalidoException {
        Password.validarPassword("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordNull_IllegalArgumentException() throws PasswordInvalidoException {
        Password.validarPassword(null);
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordSinCaracteresMinimosConDigito_void() throws PasswordInvalidoException {

        Password.validarPassword("asdasd1");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordSinCaracteresMinimosSinDigito_void() throws PasswordInvalidoException {

        Password.validarPassword("asdasd");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordSinCaracteresMaximosConDigito_void() throws PasswordInvalidoException {
        Password.validarPassword("asdasdasdasdasdasdasdasdasdasdas1");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordSinCaracteresMaximosSinDigito_void() throws PasswordInvalidoException {
        Password.validarPassword("asdasasdaasdasdasdasdasdasdasdsdad");
    }

    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordCaracteresDentroDelRangoPermitidoConDigito_void() throws PasswordInvalidoException {
        Password.validarPassword("asdasasdaa1d");

    }
    @Test(expected = PasswordInvalidoException.class)
    public void validarPassword_PasswordCaracteresDentroDelRangoPermitidoSinDigito_void() throws PasswordInvalidoException {
        Password.validarPassword("asdasasdaad");

    }
}
