
public class Pelicula extends Filmografia {

	public Pelicula (String titulo, String genero, String año){
		super(titulo, genero, año);
	}

	public String toString(){
		return espectadores + " Espectadores" + " /// Titulo: " + titulo + " /// Genero: " + genero + " /// Año: " + año;
	}
}